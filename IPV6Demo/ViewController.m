//
//  ViewController.m
//  IPV6Demo
//
//  Created by tropsci on 16/5/13.
//  Copyright © 2016年 topsci. All rights reserved.
//

#import "ViewController.h"

#import <AFNetworking/AFNetworking.h>

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UITextView *connectionContentTextView;

@property (weak, nonatomic) IBOutlet UITextView *sessionContentTextView;

@property (weak, nonatomic) IBOutlet UISwitch *connectionDomainSwitch;

@property (weak, nonatomic) IBOutlet UISwitch *sessionDomainSwitch;


@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
}


- (IBAction)urlConnectionRequestAction:(UIButton *)sender {
    NSString *url = nil;
    NSDictionary *parameters = nil;
    if (!self.connectionDomainSwitch.on) {
        url = @"http://54.175.219.8/post";
        parameters = @{@"parameters": @"IPv4 --  NSURLConnection"};
    } else {
        url = @"http://httpbin.org/post";
        parameters = @{@"parameters": @"Host Domain -- NSURLConnection"};
    }
    [self usingURLConnection:url parameters:parameters];
}


- (IBAction)urlSessionRequestAction:(UIButton *)sender {
    
    NSString *url = nil;
    NSDictionary *parameters = nil;
    
    if (!self.sessionDomainSwitch.on) {
        url = @"http://54.175.219.8/post";
        parameters = @{@"parameters": @"IPv4 --  NSURLSession"};
    } else {
        url = @"http://httpbin.org/post";
        parameters = @{@"parameters": @"Host Domain -- NSURLSession"};
    }

    [self usingURLSession:url parameters:parameters];
}


- (void)usingURLConnection:(NSString *)url parameters:(NSDictionary *)parameters {
    AFHTTPRequestOperationManager *afMangar = [AFHTTPRequestOperationManager manager];
    [afMangar.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    afMangar.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html", @"application/json", nil];
    [afMangar POST:url
        parameters:parameters
           success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
               NSLog(@"response object %@", responseObject);
               NSData *responseData = [NSJSONSerialization dataWithJSONObject:responseObject options:NSJSONWritingPrettyPrinted error:nil];
               NSString *content = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
               self.connectionContentTextView.text = content;
           }
           failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
               NSLog(@"error %@", error);
               self.connectionContentTextView.text = [error localizedDescription];
           }];
}

- (void)usingURLSession:(NSString *)url parameters:(NSDictionary *)parameters {
    AFHTTPSessionManager *sessionManager = [AFHTTPSessionManager manager];
    sessionManager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html", @"application/json", nil];
    [sessionManager POST:url
              parameters:parameters
                 success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
                     NSLog(@"response object %@", responseObject);
                     NSData *responseData = [NSJSONSerialization dataWithJSONObject:responseObject options:NSJSONWritingPrettyPrinted error:nil];
                     NSString *content = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
                     self.sessionContentTextView.text = content;
                 }
                 failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                     NSLog(@"error %@", error);
                     self.sessionContentTextView.text = [error localizedDescription];
                 }];
}

@end
